package com.thoughtworks.storageservice.file;

import com.thoughtworks.storageservice.file.model.File;
import com.thoughtworks.storageservice.file.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface FileRepository extends CrudRepository<File, String> {
  List<File> findAll();
  List<File> findByUser(User user);
  File findByIdAndUser(String id, User user);
  List<File> findByS3Key(String s3Key);
  File save(File file);
  void delete(String id);
}
