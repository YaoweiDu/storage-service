package com.thoughtworks.storageservice.file;

import com.thoughtworks.storageservice.file.model.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, String> {
  User findByEmail(String email);
  User findByToken(String token);
  User save(User user);
}
