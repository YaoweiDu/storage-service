package com.thoughtworks.storageservice.file.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;
import java.util.Objects;

public class FileRepresentation {
  private File file;

  @JsonCreator
  public FileRepresentation(File file) {
    this.file = file;
  }

  public String getId() {
    return file.getId();
  }

  public String getName() {
    return file.getName();
  }

  @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssZ")
  public Date getUpdatedAt() {
    return file.getUpdatedAt();
  }

  public boolean equals(Object object) {
    if (!(object instanceof FileRepresentation))
      return false;
    FileRepresentation representation = (FileRepresentation) object;

    return Objects.equals(representation.file.getId(), file.getId());
  }
}
