package com.thoughtworks.storageservice.file.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class FileId {
  private String id;

  @JsonCreator
  public FileId(@JsonProperty("id") String id) {
    this.id = id;
  }

  public String getId() {
    return id;
  }

  public boolean equals(Object object) {
    if (!(object instanceof FileId))
      return false;
    FileId representation = (FileId) object;

    return Objects.equals(representation.id, id);
  }
}
