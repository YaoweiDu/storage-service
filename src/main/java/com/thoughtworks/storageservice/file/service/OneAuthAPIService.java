package com.thoughtworks.storageservice.file.service;

import okhttp3.Credentials;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.http.GET;
import retrofit2.http.Header;

import java.io.IOException;

@Component
public class OneAuthAPIService {

  @Autowired
  private Retrofit retrofit;

  public boolean validate(String email, String token) {
    ValidateService service = retrofit.create(ValidateService.class);
    try {
      int code = service.validate(Credentials.basic(email, token)).execute().code();
      return code >= 200 && code < 300;
    } catch (IOException e) {
      e.printStackTrace();
      return false;
    }
  }

  interface ValidateService {
    @GET("/one_auth/api/validations")
    Call<Auth> validate(@Header("Authorization") String authorization);
  }

  class Auth {
    private String email;

    public String getEmail() {
      return email;
    }

    public void setEmail(String email) {
      this.email = email;
    }
  }
}
