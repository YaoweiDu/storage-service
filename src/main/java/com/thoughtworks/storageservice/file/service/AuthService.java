package com.thoughtworks.storageservice.file.service;

import com.thoughtworks.storageservice.file.UnauthorizedException;
import com.thoughtworks.storageservice.file.UserRepository;
import com.thoughtworks.storageservice.file.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

@Component
public class AuthService {
  @Autowired
  private UserRepository userRepository;

  @Autowired
  private JWTService jwtService;

  @Autowired
  private OneAuthAPIService oneAuthAPIService;

  @Transactional
  public User getUser(String token) {
    User user = userRepository.findByToken(token);
    if (user != null) return user;

    String email = jwtService.payload(token);
    if (oneAuthAPIService.validate(email, token)) {
      user = new User(email, token);
      userRepository.save(user);
      return user;
    } else {
      throw new UnauthorizedException("Unauthorized");
    }
  }
}
