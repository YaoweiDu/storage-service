package com.thoughtworks.storageservice.file.service;

import io.jsonwebtoken.Jwts;
import org.springframework.stereotype.Component;

@Component
public class JWTService {
  public String payload(String token) {
    return Jwts.parser()
      .parsePlaintextJwt(token.substring(0, token.lastIndexOf('.') + 1))
      .getBody()
      .replace("\"", "");
  }
}
