package com.thoughtworks.storageservice.file.service;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.*;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.URL;

@Service
public class FileStorageService {

  @Value("${AWS.S3.bucket_name}")
  private String bucketName;

  @Autowired
  private AmazonS3 amazonS3;

  public URL saveFile(MultipartFile file, String name) throws IOException {
    ObjectMetadata metadata = new ObjectMetadata();
    metadata.setContentType(file.getContentType());

    PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, name,
      file.getInputStream(), metadata).withCannedAcl(CannedAccessControlList.PublicRead);
    amazonS3.putObject(putObjectRequest);

    return amazonS3.getUrl(bucketName, name);
  }

  public byte[] getFile(String name) {
    S3ObjectInputStream objectInputStream = amazonS3.getObject(new GetObjectRequest(bucketName, name)).getObjectContent();

    byte[] bytes = new byte[0];
    try {
      bytes = IOUtils.toByteArray(objectInputStream);
    } catch (IOException e) {
      e.printStackTrace();
    }
    return bytes;
  }
}
