package com.thoughtworks.storageservice.file.service;

import com.thoughtworks.storageservice.file.*;
import com.thoughtworks.storageservice.file.model.File;
import com.thoughtworks.storageservice.file.model.User;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

@Component
public class FileService {
  static String[] allowTypes = new String[]{"txt", "png", "jpg", "bmp"};

  @Autowired
  private FileRepository fileRepository;

  @Autowired
  private FileStorageService fileStorageService;

  @Transactional
  public Stream<File> indexFiles(User user) {
    return fileRepository.findByUser(user).stream();
  }

  @Transactional
  public File getFile(String id, User user) {
    File file = fileRepository.findByIdAndUser(id, user);
    if (file == null) throw new NotFoundException("File Not Found");
    return file;
  }

  @Transactional
  public byte[] downloadFile(File file) {
    return fileStorageService.getFile(file.getS3Key());
  }

  @Transactional
  public File createFile(MultipartFile multipartFile, User user) {
    String extension = FilenameUtils.getExtension(multipartFile.getOriginalFilename());
    validateFileExtension(extension);
    String fileName = hashFileName(multipartFile, extension);
    findOrCreateFile(multipartFile, fileName);
    return fileRepository.save(new File(fileName, multipartFile.getOriginalFilename(), user));
  }

  @Transactional
  public File updateFile(String id, MultipartFile multipartFile, User user) {
    File file = fileRepository.findByIdAndUser(id, user);
    if (file == null) throw new NotFoundException("File Not Found");
    String extension = FilenameUtils.getExtension(multipartFile.getOriginalFilename());
    validateFileExtension(extension);
    String fileName = hashFileName(multipartFile, extension);
    findOrCreateFile(multipartFile, fileName);
    file.setS3Key(fileName);
    file.setName(multipartFile.getOriginalFilename());
    return fileRepository.save(file);
  }

  @Transactional
  public void deleteFile(String id, User user) {
    File file = fileRepository.findByIdAndUser(id, user);
    if (file == null) throw new NotFoundException("File Not Found");
    fileRepository.delete(file.getId());
  }

  private void findOrCreateFile(MultipartFile multipartFile, String fileName) {
    List<File> existingFile = fileRepository.findByS3Key(fileName);
    if (existingFile.isEmpty()) {
      System.out.println("uploading file...");
      try {
        fileStorageService.saveFile(multipartFile, fileName);
      } catch (IOException e) {
        e.printStackTrace();
        throw new UploadFileException("Upload file error");
      }
    }
  }

  private void validateFileExtension(String extension) {
    if (Arrays.stream(allowTypes).noneMatch(type -> type.equals(extension)))
      throw new ForbiddenTypeException("Invalid file type.");
  }

  private String hashFileName(MultipartFile multipartFile, String extension) {
    try {
      return DigestUtils.md5Hex(multipartFile.getBytes()) + "." + extension;
    } catch (IOException e) {
      e.printStackTrace();
      throw new InvalidFileContentException("Invalid multipartFile content.");
    }
  }
}
