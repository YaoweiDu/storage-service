package com.thoughtworks.storageservice.file;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class InvalidFileContentException extends RuntimeException {
  public InvalidFileContentException(String message) {
    super(message);
  }
}
