package com.thoughtworks.storageservice.file;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class ForbiddenTypeException extends RuntimeException {
  public ForbiddenTypeException(String message) {
    super(message);
  }
}
