package com.thoughtworks.storageservice.file;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
public class UploadFileException extends RuntimeException {
  public UploadFileException(String message) {
    super(message);
  }
}
