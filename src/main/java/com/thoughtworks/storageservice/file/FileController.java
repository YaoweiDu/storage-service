package com.thoughtworks.storageservice.file;

import com.thoughtworks.storageservice.file.model.File;
import com.thoughtworks.storageservice.file.model.FileId;
import com.thoughtworks.storageservice.file.model.FileRepresentation;
import com.thoughtworks.storageservice.file.model.User;
import com.thoughtworks.storageservice.file.service.AuthService;
import com.thoughtworks.storageservice.file.service.FileService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/api/files")
public class FileController {
  public static final String AUTHORIZATION = "Authorization";
  public static final String TOKEN_PREFIX = "Bearer ";

  @Autowired
  private FileService fileService;

  @Autowired
  private AuthService authService;

  @ApiOperation("List all file by user")
  @ApiImplicitParams({@ApiImplicitParam(name = "Authorization", dataType = "string", paramType = "header", value = "One Auth API Token")})
  @RequestMapping(method = RequestMethod.GET)
  @ApiResponses(value = {@ApiResponse(code = 404, message = "File not found")})
  public List<FileRepresentation> index(@RequestHeader(AUTHORIZATION) String authHeader) {
    User user = auth(authHeader);
    return fileService.indexFiles(user)
      .map(FileRepresentation::new)
      .collect(Collectors.toList());
  }

  @ApiOperation("Create a file")
  @ApiImplicitParams({@ApiImplicitParam(name = "Authorization", dataType = "string", paramType = "header", value = "One Auth API Token")})
  @RequestMapping(method = RequestMethod.POST)
  @ApiResponses(value = {@ApiResponse(code = 404, message = "File not found")})
  public FileId create(@RequestParam("file") MultipartFile file,
                       @RequestHeader("Authorization") String authHeader) {
    User user = auth(authHeader);
    return new FileId(fileService.createFile(file, user).getId());
  }

  @ApiOperation("Fetch a file")
  @ApiImplicitParams({@ApiImplicitParam(name = "Authorization", dataType = "string", paramType = "header", value = "One Auth API Token")})
  @RequestMapping(path = "{id}", method = RequestMethod.GET)
  @ApiResponses(value = {@ApiResponse(code = 404, message = "File not found")})
  public ResponseEntity<Object> show(@PathVariable("id") String id,
                                     @RequestHeader(AUTHORIZATION) String authHeader) {
    User user = auth(authHeader);

    File file = fileService.getFile(id, user);
    byte[] bytes = fileService.downloadFile(file);

    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
    headers.setContentLength(bytes.length);
    headers.setContentDispositionFormData("attachment", file.getName());

    return ResponseEntity.status(HttpStatus.OK).headers(headers).body(bytes);
  }

  @ApiOperation("Change a file")
  @ApiImplicitParams({@ApiImplicitParam(name = "Authorization", dataType = "string", paramType = "header", value = "One Auth API Token")})
  @RequestMapping(path = "{id}", method = RequestMethod.POST)
  @ApiResponses(value = {@ApiResponse(code = 404, message = "File not found")})
  public FileId update(@PathVariable("id") String id,
                       @RequestParam("file") MultipartFile file,
                       @RequestHeader(AUTHORIZATION) String authHeader) {
    User user = auth(authHeader);

    return new FileId(fileService.updateFile(id, file, user).getId());
  }

  @ApiOperation("Delete a file")
  @ApiImplicitParams({@ApiImplicitParam(name = "Authorization", dataType = "string", paramType = "header", value = "One Auth API Token")})
  @RequestMapping(path = "{id}", method = RequestMethod.DELETE)
  @ApiResponses(value = {@ApiResponse(code = 404, message = "File not found")})
  public ResponseEntity delete(@PathVariable("id") String id,
                               @RequestHeader(AUTHORIZATION) String authHeader) {
    User user = auth(authHeader);

    fileService.deleteFile(id, user);
    return new ResponseEntity(HttpStatus.OK);
  }

  private User auth(String authHeader) {
    String token = new String(Base64.getDecoder().decode(authHeader.replace(TOKEN_PREFIX, "")));
    return authService.getUser(token);
  }
}
