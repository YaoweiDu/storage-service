package com.thoughtworks.storageservice;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@SpringBootApplication
public class Application {

  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }

  @Value("${AWS.S3.access_key}")
  private String AWS_S3_ACCESS_KEY;

  @Value("${AWS.S3.secret_key}")
  private String AWS_S3_SECRET_KEY;

  @Value("${AWS.S3.region}")
  private String region;

  @Value("${one-auth-api}")
  private String ONE_AUTH_API;

  @Bean
  public AmazonS3 amazonS3() {
    BasicAWSCredentials basicAWSCredentials = new BasicAWSCredentials(AWS_S3_ACCESS_KEY, AWS_S3_SECRET_KEY);
    return AmazonS3ClientBuilder.standard().withRegion(region)
      .withCredentials(new AWSStaticCredentialsProvider(basicAWSCredentials)).build();
  }

  @Bean
  public Retrofit retrofit() {
    Retrofit build = new Retrofit.Builder()
      .baseUrl(ONE_AUTH_API)
      .addConverterFactory(GsonConverterFactory.create())
      .build();
    return build;
  }
}
