package com.thoughtworks.storageservice.file.model;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@JsonTest
public class FileIdTest {

  @Autowired
  private JacksonTester<FileId> json;

  @Test
  public void testDeserialize() throws Exception {
    String content = "{\"id\":\"123\"}";
    assertThat(this.json.parse(content))
      .isEqualTo(new FileId("123"));
    assertThat(this.json.parseObject(content).getId()).isEqualTo("123");
  }
}
