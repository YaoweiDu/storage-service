package com.thoughtworks.storageservice.file.service;

import com.thoughtworks.storageservice.Fixture;
import com.thoughtworks.storageservice.file.UnauthorizedException;
import com.thoughtworks.storageservice.file.UserRepository;
import com.thoughtworks.storageservice.file.model.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class AuthServiceTest {

  @TestConfiguration
  static class FileServiceTestContextConfiguration {
    @Bean
    public AuthService getAuthService() {
      return new AuthService();
    }
  }

  @Autowired
  private AuthService authService;

  @MockBean
  private JWTService jwtService;

  @MockBean
  private OneAuthAPIService oneAuthAPIService;

  @MockBean
  private UserRepository userRepository;

  private User user;

  @Before
  public void setUp() throws Exception {
    user = Fixture.createUserWithId();
  }

  @Test
  public void shouldReturnUserWhenUserExists() throws Exception {
    when(userRepository.findByToken(eq(user.getToken()))).thenReturn(user);

    assertEquals(authService.getUser(user.getToken()), user);
  }

  @Test
  public void shouldValidateUserByOneAuthAPI() throws Exception {
    when(userRepository.findByToken(eq(user.getToken()))).thenReturn(null);
    when(oneAuthAPIService.validate(any(String.class), any(String.class))).thenReturn(true);

    authService.getUser(user.getToken());

    verify(userRepository).save(any(User.class));
  }

  @Test(expected = UnauthorizedException.class)
  public void shouldThrowUnauthorizedExceptionWhenValidateUserFailed() {
    when(userRepository.findByToken(eq(user.getToken()))).thenReturn(null);
    when(oneAuthAPIService.validate(any(String.class), any(String.class))).thenReturn(false);
    authService.getUser(user.getToken());
  }
}
