package com.thoughtworks.storageservice.file.service;

import com.thoughtworks.storageservice.Fixture;
import com.thoughtworks.storageservice.file.FileRepository;
import com.thoughtworks.storageservice.file.ForbiddenTypeException;
import com.thoughtworks.storageservice.file.model.File;
import com.thoughtworks.storageservice.file.model.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.multipart.MultipartFile;

import java.net.URL;
import java.util.Collections;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class FileServiceTest {

  @TestConfiguration
  static class FileServiceTestContextConfiguration {
    @Bean
    public FileService getFileService() {
      return new FileService();
    }
  }

  @Autowired
  private FileService fileService;

  @MockBean
  private FileRepository fileRepository;

  @MockBean
  private FileStorageService fileStorageService;

  private File file;
  private User user;

  @Before
  public void setUp() throws Exception {
    file = Fixture.createFileWithId();
    user = Fixture.createUserWithId();
  }

  @Test
  public void indexFiles() throws Exception {
    List<File> apps = Collections.singletonList(file);
    when(fileRepository.findByUser(any(User.class))).thenReturn(apps);
    List<File> indexFiles = fileService.indexFiles(user).collect(toList());

    assertEquals(indexFiles.size(), 1);

    assertEquals(indexFiles.get(0).getS3Key(), file.getS3Key());
  }

  @Test
  public void showFile() throws Exception {
    when(fileRepository.findByIdAndUser(any(String.class), any(User.class))).thenReturn(file);

    File fetchedFile = fileService.getFile(file.getId(), user);

    assertEquals(fetchedFile.getS3Key(), file.getS3Key());
  }

  @Test
  public void createFile() throws Exception {
    // "abc" md5
    String MD5Content = "900150983cd24fb0d6963f7d28e17f72";
    MockMultipartFile mockFile = new MockMultipartFile("file", "file.txt", null, "abc".getBytes());

    when(fileRepository.save(any(File.class))).thenReturn(file);
    when(fileStorageService.saveFile(any(MultipartFile.class), eq(MD5Content + ".txt"))).thenReturn(new URL(file.getS3Key()));
    File createdFile = fileService.createFile(mockFile, user);

    assertEquals(createdFile.getS3Key(), file.getS3Key());
  }

  @Test(expected = ForbiddenTypeException.class)
  public void shouldThrowForbiddenTypeWhenNotContainAllowTypes() {
    MockMultipartFile mockFile = new MockMultipartFile("file", "file", null, "bar".getBytes());
    fileService.createFile(mockFile, user);
  }

  @Test
  public void updateFile() throws Exception {
    // "abc" md5
    String MD5Content = "900150983cd24fb0d6963f7d28e17f72";
    MockMultipartFile mockFile = new MockMultipartFile("file", "file.txt", null, "abc".getBytes());

    when(fileRepository.findByIdAndUser(any(String.class), any(User.class))).thenReturn(file);
    when(fileRepository.save(any(File.class))).thenReturn(file);
    when(fileStorageService.saveFile(any(MultipartFile.class), eq(MD5Content + ".txt"))).thenReturn(new URL(file.getS3Key()));

    File createdFile = fileService.updateFile(file.getId(), mockFile, user);

    assertEquals(createdFile.getS3Key(), file.getS3Key());
  }

  @Test
  public void deleteFile() throws Exception {
    when(fileRepository.findByIdAndUser(any(String.class), any(User.class))).thenReturn(file);
    doNothing().when(fileRepository).delete(file.getId());
    fileService.deleteFile(file.getId(), user);
    verify(fileRepository).delete(file.getId());
  }
}
