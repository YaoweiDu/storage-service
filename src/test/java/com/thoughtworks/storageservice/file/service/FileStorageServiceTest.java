package com.thoughtworks.storageservice.file.service;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.thoughtworks.storageservice.file.model.User;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.methods.HttpGet;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class FileStorageServiceTest {

  @TestConfiguration
  static class FileStorageServiceTestContextConfiguration {
    @Bean
    public FileStorageService getFileService() {
      return new FileStorageService();
    }
  }

  @Autowired
  private FileStorageService fileStorageService;

  @MockBean
  private AmazonS3 amazonS3;

  @Test
  public void saveFile() throws Exception {
    MockMultipartFile mockFile = new MockMultipartFile("file", "file",
      null, "bar".getBytes());

    when(amazonS3.putObject(any(PutObjectRequest.class))).thenReturn(null);
    URL url = new URL("http://example.com");
    when(amazonS3.getUrl(any(String.class), any(String.class))).thenReturn(url);
    assertEquals(fileStorageService.saveFile(mockFile, "abc"), url);
  }

  @Test
  public void getFile() throws Exception {
    S3Object s3Object = mock(S3Object.class);
    when(amazonS3.getObject(any(GetObjectRequest.class))).thenReturn(s3Object);

    Mockito.doReturn(
      new S3ObjectInputStream(generateInputStream(), new HttpGet())
    ).when(s3Object).getObjectContent();

    assertArrayEquals(fileStorageService.getFile("abc"), IOUtils.toByteArray(generateInputStream()));
  }

  private InputStream generateInputStream() throws IOException {
    return IOUtils.toInputStream("test", "UTF-8");
  }
}
