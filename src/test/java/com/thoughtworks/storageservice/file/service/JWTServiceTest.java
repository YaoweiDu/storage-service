package com.thoughtworks.storageservice.file.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
public class JWTServiceTest {

  @TestConfiguration
  static class FileServiceTestContextConfiguration {
    @Bean
    public JWTService getJWTService() {
      return new JWTService();
    }
  }

  @Autowired
  private JWTService jwtService;

  @Test
  public void payload() throws Exception {
    String token = "eyJhbGciOiJIUzI1NiJ9.IndhZGV1LmR1QGdtYWlsLmNvbSI.6N6ypdijNR48HU4shCfSHYBNhBXl09dKaLqqLqu0aU";
    assertEquals(jwtService.payload(token), "wadeu.du@gmail.com");
  }
}
