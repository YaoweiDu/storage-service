package com.thoughtworks.storageservice.file;

import com.thoughtworks.storageservice.Fixture;
import com.thoughtworks.storageservice.file.model.File;
import com.thoughtworks.storageservice.file.model.User;
import com.thoughtworks.storageservice.file.service.AuthService;
import com.thoughtworks.storageservice.file.service.FileService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpMethod;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.fileUpload;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(FileController.class)
public class FileControllerTest {
  @Autowired
  private MockMvc mvc;

  @MockBean
  private FileService fileService;

  @MockBean
  private AuthService authService;

  private User user;

  @Before
  public void setUp() throws Exception {
    user = Fixture.createUserWithId();
    when(authService.getUser(any(String.class))).thenReturn(user);
  }

  @Test
  public void indexFiles() throws Exception {
    List<File> files = Arrays.asList(Fixture.createFileWithId());
    when(fileService.indexFiles(eq(user)))
      .thenReturn(files.stream());

    this.mvc.perform(
      get("/api/files")
        .header("Authorization", "Bearer YW55")
    ).andExpect(status().isOk())
      .andExpect(jsonPath("$[0].id").value("123"));
  }

  @Test
  public void createFile() throws Exception {
    MockMultipartFile mockFile = new MockMultipartFile("file", "file",
      null, "bar".getBytes());
    File createdFile = Fixture.createFileWithId();

    when(fileService.createFile(any(MultipartFile.class), any(User.class))).thenReturn(createdFile);

    this.mvc.perform(
      fileUpload("/api/files")
        .file(mockFile)
        .header("Authorization", "Bearer YW55")
    )
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.id").value(createdFile.getId()));
  }

  @Test
  public void showFile() throws Exception {
    File file = Fixture.createFileWithId();
    when(fileService.getFile(any(String.class), any(User.class))).thenReturn(file);

    byte[] bytes = "abc".getBytes();
    when(fileService.downloadFile(file)).thenReturn(bytes);

    this.mvc.perform(
      get("/api/files/123")
        .header("Authorization", "Bearer YW55")
    ).andExpect(status().isOk())
      .andExpect(content().contentType("application/octet-stream"))
      .andExpect(header().string("Content-Length", String.valueOf(bytes.length)))
      .andExpect(header().string("Content-Disposition", "form-data; name=\"attachment\"; filename=\"" + file.getName() + "\""))
      .andExpect(content().bytes(bytes));
  }

  @Test
  public void updateFile() throws Exception {
    MockMultipartFile mockFile = new MockMultipartFile("file", "file",
      null, "bar".getBytes());
    File createdFile = Fixture.createFileWithId();

    when(fileService.updateFile(eq(createdFile.getId()), any(MultipartFile.class), any(User.class))).thenReturn(createdFile);
    this.mvc.perform(
      fileUpload("/api/files/" + createdFile.getId(), HttpMethod.PUT)
        .file(mockFile)
        .header("Authorization", "Bearer YW55")
    )
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.id").value(createdFile.getId()));
  }

  @Test
  public void deleteFile() throws Exception {
    File createdFile = Fixture.createFileWithId();

    doNothing().when(fileService).deleteFile(eq(createdFile.getId()), any(User.class));
    this.mvc.perform(
      delete("/api/files/" + createdFile.getId())
        .header("Authorization", "Bearer YW55")
    )
      .andExpect(status().isOk());
  }
}
