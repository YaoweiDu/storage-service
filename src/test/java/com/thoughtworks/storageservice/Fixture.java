package com.thoughtworks.storageservice;

import com.thoughtworks.storageservice.file.model.File;
import com.thoughtworks.storageservice.file.model.User;

public class Fixture {
  public static File createFile() {
    return new File("http://s3.com/R2S-0.0.1.ipa", "R2s-0.0.1.ipa");
  }

  public static File createFileWithId() {
    return new File("123", "http://s3.com/R2S-0.0.1.ipa", "R2s-0.0.1.ipa");
  }

  public static User createUserWithId() {
    return new User("123", "fake@gmail.com", "token");
  }
}
